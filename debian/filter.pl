#!/usr/bin/perl
# filter of help2man output
use strict;
use warnings;

while ( <STDIN> ) {
    #multiline regex
    undef $/;
    # substitude unrecognized version string with the version
    $_ =~ s/(\.SH\sDESCRIPTION\n)(No\sinput\ssequence.*?)(\n)/$1\.TP$3/gxms;
    $_ =~ s/(\*{3}\n)(Options):\n\.TP/$1.'.SH '.uc($2)/gmsex;
    $_ =~ s/(\.PP\n)(Options)(\s.*?):(\n)/$1.'.SH '.uc($2).$3.$4/msex;
    $_ =~ s{(\.IP\n)(Input|Presets|Alignment|Scoring|Reporting|
	Effort|Paired-end|Output|Performance|Other)(:\n)(\.TP|\.HP\n)}
	{.SS $2$3$4}gmsx;
    $_ =~ s/(\.TP\n)(Presets:)\n/\.SS $2\n/xms;
    $_ =~ s{(\(Note:\sfor.*?\.)(\n\.IP)}{
		my ($p,$q) = ($1,$2);
		$p =~ s/(\n)/$1 /gxms;
		$p = ' '.$p.$q;
	   }msex;
    $_ =~ s/.*(\soutput\sfile\sspecified\!|No\sindex\sname\sgiven\!)//g;
    $_ =~ s/\(ERR\):.*$//g;
    $_ =~ s/(Usage:\n\.IP)/.SH\nUSAGE/g;
    $_ =~ s/\*{3}\sWarning.*\sinstead\.//gxms;
    print $_;
}
