Source: bowtie2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Alexandre Mestiashvili <mestia@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               architecture-is-64-bit,
               architecture-is-little-endian,
               help2man,
               pandoc <!nodoc>,
               libtbb-dev,
               python3,
               liblocal-lib-perl,
               libtest-deep-perl <!nocheck>,
               libclone-perl <!nocheck>,
               libfile-which-perl <!nocheck>,
               zlib1g-dev,
               libsimde-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/bowtie2
Vcs-Git: https://salsa.debian.org/med-team/bowtie2.git
Homepage: https://bowtie-bio.sourceforge.net/bowtie2
Rules-Requires-Root: no

Package: bowtie2
Architecture: any
Depends: python3,
         ${misc:Depends},
         ${shlibs:Depends},
         ${perl:Depends}
Suggests: bowtie2-examples
Built-Using: ${simde:Built-Using}
Description: ultrafast memory-efficient short read aligner
 is an ultrafast and memory-efficient tool for aligning sequencing reads
 to long reference sequences. It is particularly good at aligning reads
 of about 50 up to 100s or 1,000s of characters, and particularly good
 at aligning to relatively long (e.g. mammalian) genomes.
 .
 Bowtie 2 indexes the genome with an FM Index to keep its memory footprint
 small: for the human genome, its memory footprint is typically
 around 3.2 GB. Bowtie 2 supports gapped, local, and paired-end alignment modes

Package: bowtie2-examples
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: bowtie2
Enhances: bowtie2
Description: Examples for bowtie2
 An ultrafast and memory-efficient tool for aligning sequencing reads
 to long reference sequences. It is particularly good at aligning reads
 of about 50 up to 100s or 1,000s of characters, and particularly good
 at aligning to relatively long (e.g. mammalian) genomes.
 .
 Bowtie 2 indexes the genome with an FM Index to keep its memory footprint
 small: for the human genome, its memory footprint is typically
 around 3.2 GB. Bowtie 2 supports gapped, local, and paired-end alignment modes
 .
 This package provides some example data to work with bowtie2.
