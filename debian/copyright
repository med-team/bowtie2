Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Bowtie2
Upstream-Contact: Ben Langmead <langmea@cs.jhu.edu>
Source: https://github.com/BenLangmead/bowtie2/releases

Files: *
Copyright: © 2009-2016 Ben Langmead, C. Trapnell, M. Pop, Steven L. Salzberg
License: GPL-3.0+

Files: fast_mutex.h
Copyright: © 2010-2012, Marcus Geelnard
License: Zlib

Files: ls.h
Copyright: © 1999, N. Jesper Larsson
License: other
 This file contains an implementation of the algorithm presented in "Faster
 Suffix Sorting" by N. Jesper Larsson (jesper@cs.lth.se) and Kunihiko
 Sadakane (sada@is.s.u-tokyo.ac.jp).
 .
 This software may be used freely for any purpose. However, when distributed,
 the original source must be clearly stated, and, when the source code is
 distributed, the copyright notice must be retained and any alterations in
 the code must be clearly marked. No warranty is given regarding the quality
 of this software.

Files: debian/*
Copyright: © 2016 Alexandre Mestiashvili <alex@biotec.tu-dresden.de>
License: GPL-3.0+

License: GPL-3.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
